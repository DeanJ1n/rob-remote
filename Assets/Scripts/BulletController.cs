﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    //private fields
    private Vector3 originalPos;
    private Rigidbody rd;
    private bool fired = false;

    //serialized fields
    [SerializeField] float period;
    [SerializeField] float gunPower;

	// Use this for initialization
	void Start () {
        //get the original position of the bullet
        originalPos = gameObject.transform.position;
        rd = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        //check if a bullet is already fired by this gun
        if (!fired)
        {
            fired = true;
            Invoke("fireBullet", period);
        }
	}

    private void fireBullet()
    {
        rd.AddRelativeForce(Vector3.up * gunPower * Time.deltaTime * 60);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("gun"))
        {
            //empty the velocity
            rd.velocity = Vector3.zero;
            rd.angularVelocity = Vector3.zero;

            //set bullet back to its original position
            gameObject.transform.position = originalPos;

            //enable another fire
            fired = false;
        }
    }
}
