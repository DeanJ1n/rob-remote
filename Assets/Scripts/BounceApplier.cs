﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceApplier : MonoBehaviour {

    [SerializeField] Vector3 force;

    private AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision other)
    {
        //add a force to the palyer object
        other.gameObject.GetComponent<Rigidbody>().AddForce(force);

        //play bouncing effect
        source.Play();
    }

}
