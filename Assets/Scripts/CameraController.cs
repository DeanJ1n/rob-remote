﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    //cached references
    [SerializeField] GameObject player;
    private Vector3 offset;

	// Use this for initialization
	void Start () {
        //get the original offset of position regards to the player.
        offset = transform.position - player.transform.position;
	}
	
	// late update runs each frame after all items are processed in update.
	void LateUpdate () {
        transform.position = player.transform.position + offset;
	}
}
