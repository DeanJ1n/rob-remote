﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //private fields
    private int maxCount = 0;
    private bool isAlive = true;
    private AudioSource source;

    //cached references
    private Rigidbody rb;

    //design levers
    [SerializeField] float speed = 1f;
    [SerializeField] ParticleSystem explosion;
    [SerializeField] AudioClip collect;
    [SerializeField] AudioClip winning;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        source = GetComponent<AudioSource>();
    }

    //fixed upadate method for physics manipulation
    void FixedUpdate()
    {
        if (isAlive)
        {
            float moveHorizontal = Input.GetAxis("Horizontal") * speed;
            float moveVertical = Input.GetAxis("Vertical") * speed;

            //add force to the rigid body in order to make it move
            rb.AddForce(new Vector3(moveHorizontal, 0.0f, moveVertical));
        }
    }

    //manipulate the collisions happen to the object
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("pickup"))
        {
            //play audio effect on collecting pickups
            source.PlayOneShot(collect);

            //when the game object is a pick up, deactivate the object
            other.gameObject.SetActive(false);
        }else if (other.CompareTag("deadly"))
        {
            isAlive = false;
            source.Play();
            explosion.Play();
        }
    }

    //adding up the number of total collectables
    public void countPickUp()
    {
        maxCount++;
    }

    //a public method for other classes to access player alive status
    public bool getAliveStatus()
    {
        return isAlive;
    }

    internal void playWinEffect()
    {
        source.PlayOneShot(winning);
    }
}
