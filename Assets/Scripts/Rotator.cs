﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    private SceneLoader sl;
    Vector3 rotation;

	// Use this for initialization
	void Start () {
        //get the scene loader object to load scene and do count
        sl = FindObjectOfType<SceneLoader>();
        sl.countPickUps();

        //randomly generate rotation vector
        rotation = new Vector3(Random.Range(-90, 90), Random.Range(-90, 90), Random.Range(-90, 90));
	}
	
	// Update is called once per frame
	void Update () {
        //rotate the pickup
        transform.Rotate(rotation * Time.deltaTime);
	}

    private void OnTriggerEnter(Collider other)
    {
        sl.pickUpDown();
    }
}
