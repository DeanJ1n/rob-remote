﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class SceneLoader : MonoBehaviour {

    //private fields
    private int pickups;
    private int count;
    private PlayerController player;

    //serialized fields
    [SerializeField] Text countText;
    [SerializeField] Text winText;
    [SerializeField] float levelLoadDelay;
    [SerializeField] float deathLoadDelay;

    // Use this for initialization
    void Start () {
        //set pickups to 0 at the beginning of the scene
        SetCountText();
        winText.text = "";

        player = FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        //check if user pressed R
        if (Input.GetKeyDown(KeyCode.R))
        {
            //reload scene
            winText.text = "Reloading...";
            Invoke("reloadScene", levelLoadDelay);
        }

        //check if the player is destoryed by deadly items
        if (!player.getAliveStatus())
        {
            //the player failed to pass the level
            playerFailed();
        }
    }

    private void reloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
    }

    //here is the method that every pickup will call when they are in the scene
    public void countPickUps()
    {
        pickups++;
    }

    //here is a method that every pickup will call when they are destroyed
    public void pickUpDown()
    {
        pickups--;
        count++;
        SetCountText();

        //check if there are no pickups any more
        if(pickups == 0)
        {
            //modify the win text and load next scene
            winText.text = "YOU WON!";
            player.playWinEffect();
            Invoke("loadNextScene", levelLoadDelay);
        }
    }

    public void playerFailed()
    {
        //set win text to somthing else
        winText.text = "Try again";

        Invoke("reloadScene", deathLoadDelay);
    }

    private void loadNextScene()
    {
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings);
    }
}
