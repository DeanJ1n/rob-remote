﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDetector : MonoBehaviour {

    private SceneLoader sl;

	// Use this for initialization
	void Start () {
        //get the scene loader
        sl = FindObjectOfType<SceneLoader>();
	}

    //detect if anything falls onto the collider
    private void OnTriggerEnter(Collider other)
    {
        sl.playerFailed();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
